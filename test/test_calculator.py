from app import calculator


def test_add():
    assert calculator.add(1, 2) == 3
    assert calculator.add(0, 0) == 0


def test_sub():
    assert calculator.subtract(1, 2) == -1
    assert calculator.subtract(2, 2) == 0
